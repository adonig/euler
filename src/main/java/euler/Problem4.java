package euler;

/**
 * A palindromic number reads the same both ways. The largest palindrome made
 * from the product of two 2-digit numbers is 9009 = 91 x 99.
 * <p/>
 * Find the largest palindrome made from the product of two 3-digit numbers.
 */
public final class Problem4 {

    public static void main(String[] args) {
        int[] threeDigitNumbers = new int[999];
        for (int i = 999; i >= 100; --i) {
            threeDigitNumbers[999 - i] = i;
        }
        int maxPalindrome = Integer.MIN_VALUE;
        for (int n : threeDigitNumbers) {
            for (int m : threeDigitNumbers) {
                int product = n * m;
                if (product > maxPalindrome && isPalindrome(product)) {
                    maxPalindrome = product;
                }
            }
        }
        System.out.println("the largest palindrome made from the product "
                + "of two 3-digit numbers: " + maxPalindrome);
    }

    private static boolean isPalindrome(int n) {
        String s = String.valueOf(n);
        int i = 0;
        int j = s.length() - 1;
        boolean palindrome = true;
        while (palindrome && i < j) {
            if (s.charAt(i) != s.charAt(j)) {
                palindrome = false;
            } else {
                ++i;
                --j;
            }
        }
        return palindrome;
    }
}
