package euler;

/**
 * The sum of the squares of the first ten natural numbers is,
 * <p/>
 * 1^2 + 2^2 + ... + 10^2 = 385
 * <p/>
 * The square of the sum of the first ten natural numbers is,
 * <p/>
 * (1 + 2 + ... + 10)^2 = 552 = 3025
 * <p/>
 * Hence the difference between the sum of the squares of the first ten
 * natural numbers and the square of the sum is 3025 - 385 = 2640.
 * <p/>
 * Find the difference between the sum of the squares of the first one
 * hundred natural numbers and the square of the sum.
 */
public final class Problem6 {

    public static void main(String[] args) {
        int n = 100;
        int sum = n * (n + 1) / 2;
        int squareOfSum = sum * sum;
        int sumOfSquares = 0;
        for (int i = 1; i <= n; ++i) {
            sumOfSquares += i * i;
        }
        int difference =  squareOfSum - sumOfSquares;
        System.out.println("the difference between the sum of the squares "
                + "of the first one hundred natural numbers and the square "
                + "of the sum: " + difference);
    }
}
