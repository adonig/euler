package euler;

/**
 * If we list all the natural numbers below 10 that are multiples of 3 or 5,
 * we get 3, 5, 6 and 9. The sum of these multiples is 23.
 * <p/>
 * Find the sum of all the multiples of 3 or 5 below 1000.
 */
public final class Problem1 {

    public static void main(String[] args) {
        final int n = 999;
        final int sum = (n / 3) * (n / 3 + 1) / 2 * 3 // add all multiples of 3 below 1000
                + (n / 5) * (n / 5 + 1) / 2 * 5       // add all multiples of 5 below 1000
                - (n / 15) * (n / 15 + 1) / 2 * 15;   // subtract all multiples of 15 below 1000
        System.out.println("sum of all the multiples of 3 or 5 below 1000: " + sum);
    }
}
