package euler;

/**
 * The prime factors of 13195 are 5, 7, 13 and 29.
 * <p/>
 * What is the largest prime factor of the number 600851475143 ?
 */
public final class Problem3 {

    public static void main(String[] args) {
        long l = 7;
        System.out.print("the largest prime factor of the number " + l);
        long maxFactor = (l % 2 == 0) ? 2 : Integer.MIN_VALUE;
        while (l % 2 == 0 && l > 1) {
            l /= 2;
        }
        long divisor = 3;
        while (divisor <= l) {
            while (l % divisor == 0 && l > 1) {
                if (divisor > maxFactor) {
                    maxFactor = divisor;
                }
                l /= divisor;
            }
            divisor += 2;
        }
        System.out.println(": " + maxFactor);
    }
}
