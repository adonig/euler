package euler;

import java.util.Iterator;

/**
 * The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
 * <p/>
 * Find the sum of all the primes below two million.
 */
public final class Problem10 {

    public static void main(String[] args) {
        final Iterator<Integer> primes = new Problem7.Primes().iterator();
        long sum = 0;
        while (true) {
            final int prime = primes.next();
            if (prime >= 2e6) {
                break;
            } else {
                sum += prime;
            }
        }
        System.out.println("the sum of all the primes below two million: " + sum);
    }
}
