package euler;

import java.util.Iterator;

/**
 * By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can
 * see that the 6th prime is 13.
 * <p/>
 * What is the 10 001st prime number?
 */
public final class Problem7 {

    public static class Primes implements Iterable<Integer> {

        private boolean[] sieve;

        private int base = 2;

        public Primes() {
            this(1);
        }

        public Primes(int n) {
            if (n < 1) {
                throw new IllegalArgumentException("n must be positive");
            }
            sieve = new boolean[n + 2];
            for (int i = 2; i < sieve.length; ++i) {
                sieve[i] = true;
            }
        }

        @Override
        public Iterator<Integer> iterator() {
            return new Iterator<Integer>() {

                private int cursor = 2;

                @Override
                public boolean hasNext() {
                    return true; // proved by Euclid (c. 300 BC)
                }

                @Override
                public Integer next() {
                    // By contract at this point the cursor points at the base,
                    // at a prime number or at sieve.length. If the latter is
                    // the case, the sieve needs a resize and its size is doubled.
                    assert (cursor == base || cursor == sieve.length || sieve[cursor]);
                    if (cursor == sieve.length) {
                        base = sieve.length;
                        final boolean[] newSieve = new boolean[2 * sieve.length];
                        System.arraycopy(sieve, 0, newSieve, 0, sieve.length);
                        for (int i = base; i < newSieve.length; ++i) {
                            newSieve[i] = true;
                        }
                        sieve = newSieve;
                    }
                    // If the cursor points at the base it is either the first
                    // call to next() or there was a resize of the sieve before.
                    // In both cases parts of the sieve are inconsistent and
                    // there is need to update those parts of the sieve.
                    if (cursor == base) {
                        final int squareRootOfSieveLength = (int) Math.sqrt(sieve.length);
                        for (int i = 2; i <= squareRootOfSieveLength; ++i) {
                            if (sieve[i]) {
                                int j = i * i;
                                if (j < base) {
                                    j += ((base - j) - (base - j) % i);
                                }
                                while (j < sieve.length) {
                                    sieve[j] = false;
                                    j += i;
                                }
                            }
                        }
                        // After updating the inconsistent parts of the sieve,
                        // the cursor has to be incremented until it hits a
                        // prime number.
                        while (!sieve[cursor]) {
                            ++cursor;
                        }
                    }
                    // By contract at this point the cursor points at a prime
                    // number. This prime number should be returned by next().
                    // But To fulfill the contract for a possibly following call
                    // to next(), the cursor must be incremented first, until
                    // it points either to a prime number or to sieve.length.
                    assert sieve[cursor];
                    final int prime = cursor;
                    do {
                        ++cursor;
                    } while (cursor < sieve.length && !sieve[cursor]);
                    return prime;
                }

                @Override
                public void remove() {
                    throw new UnsupportedOperationException();
                }
            };
        }
    }

    public static void main(String[] args) {
        final int n = 10001;
        final Iterator<Integer> primes = new Primes((int)1e6).iterator();
        for (int i = 0; i < 4e18; ++i) {
            System.out.println(primes.next());
        }
        System.out.println("the " + n + "st prime number: " + primes.next());
    }
}
