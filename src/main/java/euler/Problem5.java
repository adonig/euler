package euler;

/**
 * 2520 is the smallest number that can be divided by each of the numbers
 * from 1 to 10 without any remainder.
 * <p/>
 * What is the smallest positive number that is evenly divisible by all of
 * the numbers from 1 to 20?
 */
public final class Problem5 {

    public static void main(String[] args) {
        outer:
        for (int i = 2520; ; ++i) {
            for (int d = 20; d >= 11; --d) {
                if (i % d != 0) {
                    continue outer;
                }
            }
            System.out.println("the smallest positive number that is evenly "
                    + "divisible by all of the numbers from 1 to 20: " + i);
            break;
        }
    }
}
