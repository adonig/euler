package euler;

/**
 * The following iterative sequence is defined for the set of positive
 * integers:
 * <p/>
 * n -> n/2 (n is even)
 * n -> 3n + 1 (n is odd)
 * <p/>
 * Using the rule above and starting with 13, we generate the following
 * sequence:
 * <p/>
 * 13  40  20  10  5  16  8  4  2  1
 * <p/>
 * It can be seen that this sequence (starting at 13 and finishing at 1)
 * contains 10 terms. Although it has not been proved yet (Collatz Problem),
 * it is thought that all starting numbers finish at 1.
 * <p/>
 * Which starting number, under one million, produces the longest chain?
 * <p/>
 * NOTE: Once the chain starts the terms are allowed to go above one million.
 */
public final class Problem14 {

    public static void main(final String[] args) {
        int maxHeight = Integer.MIN_VALUE;
        int maxN = 0;
        for (int n = 1; n < 1000000; ++n) {
            int height = 1;
            long m = n;
            while (m > 1) {
                m = (m % 2 == 0) ? (m / 2) : (3 * m + 1);
                ++height;
            }
            if (height > maxHeight) {
                maxHeight = height;
                maxN = n;
            }
        }
        System.out.format("starting number: %d%n", maxN);
    }
}
