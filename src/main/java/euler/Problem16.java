package euler;

import java.math.BigInteger;

/**
 * 2^15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.
 * <p/>
 * What is the sum of the digits of the number 2^1000?
 */
public final class Problem16 {

    public static void main(final String[] args) {
        BigInteger n = BigInteger.valueOf(2).pow(1000);
        int sum = 0;
        while (n.compareTo(BigInteger.ZERO) > 0) {
            sum += n.mod(BigInteger.TEN).intValue();
            n = n.divide(BigInteger.TEN);
        }
        System.out.println("sum of the digits of the number 2^1000: " + sum);
    }
}
