package euler;

import java.math.BigInteger;

/**
 * Starting in the top left corner of a 2x2 grid, there are 6 routes
 * (without backtracking) to the bottom right corner.
 * <p/>
 * How many routes are there through a 20x20 grid?
 */
public final class Problem15 {

    public static void main(final String[] args) {
        System.out.println("routes: " + bincoeff(40, 20));
    }

    private static BigInteger bincoeff(final int n, final int k) {
        return fac(n).divide(fac(k).multiply(fac(n - k)));
    }

    private static BigInteger fac(final int n) {
        return fac(n, BigInteger.ONE);
    }

    private static BigInteger fac(final int n, final BigInteger acc) {
        return (n <= 1) ? acc : fac(n - 1, acc.multiply(BigInteger.valueOf(n)));
    }
}
